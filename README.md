# src2one

![](top.png)

[[_TOC_]]

## Overview

This simple Raku script copies the contents of source files into a single file.
This can be useful for uploading to online services like ChatGPT, Perplexity, Claude, etc.
It's designed for quick use, speeding up development alongside AI. Just run src2one, and voila! All your sources are in one file, ready to be uploaded.

## Dependencies

* [rakudo](https://rakudo.org/downloads) (with zef; [rakubrew](https://rakubrew.org))

## Installation

0. Install Raku (see [dependencies](#Dependencies))
1. Clone the repository `git clone https://github.com/monnef/src2one.git`
2. Install dependencies with `zef install --/test File::Find JSON::Fast YAMLish Data::TextOrBinary`
3. Setup the [alias](#Alias)

### Alias

Adding an alias is strongly recommended.
The rest of this document (and the script itself) assumes the alias has been added.

Run the following command in this project's directory:
```sh
$ ./src2one.raku alias
```

Then follow the instructions in the output.

## Usage

To generate a config file for a given project, run the following command in the project directory:
```sh
$ src2one init
```

Then, to generate the sources file, run the following command in the project directory:
```sh
$ src2one
Using YAML config file: src2one.yaml
About to crawl the following items: "package.json", "README.md", "DEV.md", ".gitignore", "be", "fe-pplx", "docker".
Found 15 file(s) to process, 15204 file(s) ignored.
Writing file contents into sources.txt... Done.
```

## Configuration

The script looks for configuration in the following files, in order of precedence:
- `src2one.yaml`
- `src2one.json`
- `copy-sources-config.json` (deprecated)

If no configuration file is found, the script will terminate with an error.

### Configuration File Structure

The configuration file should contain the following keys:
- `file-name`: The name of the output file (default: `sources.txt`).
- `input-names`: An array of directories or files to include (default: `['.']`).
- `ignore-patterns`: An array of [Raku regular expressions](https://docs.raku.org/language/regexes) to ignore (default: `[]`).

Example `src2one.yaml`:
```yaml
file-name: 'sources.txt'
input-names:
  - 'src'
  - 'lib'
ignore-patterns:
  - '^"sources.txt"$'
```

## License

GPLv3