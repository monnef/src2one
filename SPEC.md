# src2one - Source Code Consolidation Utility

## 1. Project Overview

### 1.1 Purpose
The `src2one` utility is a tool designed to consolidate source code files from a project into a single text file. Its primary use case is to facilitate easy sharing of entire project source code with AI services like ChatGPT, Perplexity, and Claude, supporting quick development alongside AI.

### 1.2 Key Objectives
- Aggregate source code files from specified directories
- Ignore binary and unwanted files
- Generate a single, readable output file
- Provide flexible configuration options

## 2. Functional Requirements

### 2.1 Configuration
The utility SHALL support configuration through one of the following files (in order of precedence):
1. `src2one.yaml`
2. `src2one.json`
3. `copy-sources-config.json` (deprecated)

#### 2.1.1 Configuration File Structure
The configuration file MUST support the following keys:
- `file-name`: Output file name (default: `sources.txt`)
- `input-names`: Array of directories or files to include (default: `['.']`)
- `ignore-patterns`: Array of regular expressions for file exclusion (default: `['^"sources.txt"$']`)

### 2.2 File Processing
The utility SHALL:
- Recursively find files in specified input directories
- Detect and skip binary files
- Append text files to the output file with a header indicating the file path
- Ignore files matching specified ignore patterns

### 2.3 Command-Line Interface
The utility SHALL support the following commands and options:
- `init`: Create a default configuration file
- `alias`: Generate shell alias commands for both bash and zsh
- `-h, --help`: Display help information
- `-v, --verbose`: Enable verbose logging
- `-d, --debug`: Enable debug logging (implies verbose)

## 3. Non-Functional Requirements

### 3.1 Performance
- The utility SHOULD efficiently process large numbers of files
- Memory usage SHOULD be optimized for projects with many source files

### 3.2 Compatibility
- MUST support major operating systems (Linux, macOS, Windows)
- SHOULD work with various programming language projects

### 3.3 Error Handling
- MUST provide clear error messages for configuration issues
- SHOULD gracefully handle file reading and processing errors

## 4. Input Validation

### 4.1 Configuration Validation
- Configuration MUST contain 'input-names' array
- `input-names` MUST be a non-empty array
- Paths in `input-names` MUST be valid directories or files
- `ignore-patterns` MUST be valid regular expressions

### 4.2 File Processing Validation
- Ignore binary files
- Skip files matching ignore patterns
- Ensure output file is writable

## 5. Output Specification

### 5.1 Output File Format
- Header with generation timestamp in format: "Generated on: [timestamp]"
- Full project path
- Each source file preceded by a separator: `==== /path/to/file ====`
- Source file contents
- Newline between file contents

## 6. Security Considerations
- SHOULD NOT include sensitive files (e.g., credentials, private keys)
- Configuration SHOULD allow excluding sensitive directories
- Default ignore pattern prevents inclusion of output file

## 7. Extensibility
- Configuration format SHOULD allow easy addition of new options
- Code SHOULD be modular to support future feature additions
- Implementation uses separate subroutines for each major function

## 8. Deployment
- SHOULD be installable via package managers
- SHOULD have minimal external dependencies

## 9. Testing Considerations
- Unit tests for file detection
- Integration tests for various project structures
- Configuration file parsing tests
- Error handling scenarios
- Test directory structure includes legacy and YAML test cases

## 10. Documentation Requirements
- Clear README with installation instructions
- Examples of configuration file usage
- Explanation of command-line options
- Detailed alias setup instructions for bash and zsh
