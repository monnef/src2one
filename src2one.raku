#!/usr/bin/env raku
## copy-sources-to-one-file/src2one.raku
# This is a simple Raku script to copy sources to one file
# for use upload to online service like ChatGPT, Perplexity, Claude etc.
# Configured in `src2one.yaml`, `src2one.json`, or `copy-sources-config.json` (deprecated)

use File::Find;
use JSON::Fast;
use YAMLish;
use Data::TextOrBinary;

constant CONFIG-YAML = 'src2one.yaml';
constant CONFIG-JSON = 'src2one.json';
constant CONFIG-JSON-LEGACY = 'copy-sources-config.json';

my @config-files = CONFIG-YAML, CONFIG-JSON, CONFIG-JSON-LEGACY;
my %config;

sub load-config {
    for @config-files -> $file {
        if $file.IO.e {
            given $file {
                when CONFIG-YAML {
                    say "Using YAML config file: $file";
                    %config = load-yaml($file.IO.slurp);
                    last;
                }
                when CONFIG-JSON | CONFIG-JSON-LEGACY {
                    if $file ~~ CONFIG-JSON-LEGACY {
                        note "DEPRECATED: Using legacy JSON config file: $file. Please migrate to {CONFIG-YAML} or {CONFIG-JSON}.";
                    } else {
                        say "Using JSON config file: $file";
                    }
                    %config = from-json($file.IO.slurp);
                    last;
                }
            }
        }
    }
    die "No config file found. Please provide either {CONFIG-YAML} or {CONFIG-JSON}. You can create one with `src2one init`" unless %config;
}

sub validate-config(%config) {
    die "Config file must have 'input-names' array." unless %config<input-names>.WHAT ~~ List;
}

my %DEFAULT_CONFIG_VALUES = (
'file-name' => 'sources.txt',
'input-names' => ['.'],
'ignore-patterns' => ['^"sources.txt"$']
);

sub create-default-config {
    if CONFIG-YAML.IO.e || CONFIG-JSON.IO.e {
        say "Config file already exists. Not overriding.";
    } else {
        spurt CONFIG-YAML, save-yaml(%DEFAULT_CONFIG_VALUES);
        say "Default config created in {CONFIG-YAML}";
    }
}

sub show-help {
    say q:to/END/;
    Simple Raku script to copy sources to one file, written by monnef.
    Usage: src2one.raku [options]
    
    Options:
      init          Create a default config file in the current directory
      alias         Generate alias command for the script
      -h, --help    Show this help message
      -v, --verbose Enable verbose output
      -d, --debug   Enable debug output (implies verbose)
    END
}

sub append-to-file($file, %context) {
    %context<verbose-log>("Appending: $file");
    if $file.IO.f {
        my $out-file-name = %context<file-name>;
        try {
            if is-binary($file) {
                %context<verbose-log>("Skipped binary file: $file");
            } else {
                spurt $out-file-name, "==== $file ====\n" ~ $file.IO.slurp ~ "\n\n", :append;
            }
            CATCH {
                default {
                    note "Error processing file $file: $_";
                }
            }
        }
    }
}

sub is-binary($file) {
    my $buf = $file.IO.open(:bin).read(1024);
    return !is-text($buf);
}

sub should-ignore($file, %context) {
    for @(%context<ignore-patterns>) -> $pattern {
        return True if $file ~~ $pattern;
    }
    return False;
}

sub generate-alias {
    my $script-path = $*PROGRAM.absolute;
    my $alias-name = 'src2one';
    my $alias-command = qq:to/END/;
# Add the following alias to your shell configuration file:
alias $alias-name="$script-path"

# To add the alias to your .bashrc file, run:
echo 'alias $alias-name="$script-path"' >> ~/.bashrc

# To add the alias to your .zshrc file, run:
echo 'alias $alias-name="$script-path"' >> ~/.zshrc
END
    say $alias-command;
}

sub MAIN(Str $command = '', Bool :v(:$verbose), Bool :d(:$debug), Bool :h(:$help)) {
    if $help {
        show-help;
        exit;
    }

    given $command {
        when 'init' {
            create-default-config;
            exit;
        }
        when 'alias' {
            generate-alias;
            exit;
        }
        default {
            load-config;
            validate-config(%config);
        }
    }

    my %context = (
    'file-name' => %config<file-name> // %DEFAULT_CONFIG_VALUES<file-name>,
    'input-names' => @(%config<input-names> // %DEFAULT_CONFIG_VALUES<input-names>),
    'ignore-patterns' => @(%config<ignore-patterns> // %DEFAULT_CONFIG_VALUES<ignore-patterns>).map({ /<$_>/ }),
    'verbose' => $verbose || $debug,
    'debug' => $debug,
    'verbose-log' => sub ($msg) {
        say "[LOG] " ~ $msg if $verbose || $debug;
    },
    'debug-log' => sub ($msg) {
        say "[DEBUG] " ~ $msg if $debug;
    }
    );

    %context<file-name>.IO.unlink;

    my $current-time = DateTime.now;
    my $project-path = $*CWD.absolute;

    my $header = qq:to/END/;
    Generated on: $current-time
    Full project path: $project-path
    END

    spurt %context<file-name>, $header ~ "\n";

    %context<debug-log>("Context: " ~ %context.raku);

    say "About to crawl the following items: { %context<input-names>.map( { "\"$_\"" }).join(', ') }.";
    $*OUT.flush;
    my $ignored = 0;
    my @files = (gather for @(%context<input-names>) -> $item {
        %context<verbose-log>("Processing input name: $item");
        if $item.IO.d {
            my @found = find(dir => $item).grep(*.IO.f).sort(*.basename);
            my @without-ignored = @found.grep({ !should-ignore($_, %context) });
            $ignored += @found - @without-ignored;
            take @without-ignored;
        } elsif $item.IO.f {
            take $item;
        }
    }).flat;
    say "Found { @files.elems } file(s) to process, { $ignored } file(s) ignored.";
    print "Writing file contents into { %context<file-name> }... ";
    say '' if %context<verbose>;
    $*OUT.flush;
    append-to-file($_, %context) for @files;
    say "Done.";
}
